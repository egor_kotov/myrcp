package org.recipecatalogue.generic.utils;

/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 27.04.12
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
/**
 * Provides the set of available <code>{@link java.security.MessageDigest}</code> algorithms as described
 * <a href="http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest">here</a>.
 *
 * @author Rod Odin
 */
public enum MessageDigestAlgorithm
{
    MD2    ("MD2"    ),
    MD5    ("MD5"    ),
    SHA1   ("SHA-1"  ),
    SHA256 ("SHA-256"),
    SHA384 ("SHA-384"),
    SHA512 ("SHA-512");

    MessageDigestAlgorithm(String algorithmId)
    {
        this.algorithmId = algorithmId;
    }

    public String getAlgorithmId()
    {
        return algorithmId;
    }

    private final String algorithmId;
}
