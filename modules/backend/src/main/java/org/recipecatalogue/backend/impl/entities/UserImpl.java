package org.recipecatalogue.backend.impl.entities;

/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 27.04.12
 * Time: 21:23
 * To change this template use File | Settings | File Templates.
 */

import org.hibernate.annotations.GenericGenerator;
import org.recipecatalogue.backend.entities.User;

import javax.persistence.*;

/**
 * Description.
 *
 * @author Rod Odin
 */
@Entity
@Table(name = "users")
public class UserImpl
        extends AbstractEntityImpl
        implements User
{
// Constructors --------------------------------------------------------------------------------------------------------

    /**
     * Required for Hibernate.
     */
    public UserImpl()
    {
    }

// Getters/Setters -----------------------------------------------------------------------------------------------------

    @Id
    @Column(name = "user_id", nullable = false, unique = true)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    public Long getId()
    {
        return super.getId();
    }

    @Column(name = "email", nullable = false, unique = true)
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
        setModified(true);
    }

    @Transient
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
        setModified(true);
    }

    @Column(name = "password_signature", nullable = false, length = 32)
    public String getPasswordSignature()
    {
        return passwordSignature;
    }

    public void setPasswordSignature(String passwordSignature)
    {
        this.passwordSignature = passwordSignature;
        setModified(true);
    }

    @Column(name = "first_name", nullable = true, unique = false)
    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
        setModified(true);
    }

    @Column(name = "last_name", nullable = true, unique = false)
    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
        setModified(true);
    }

    @Column(name = "disabled")
    public boolean isDisabled()
    {
        return disabled;
    }

    public void setDisabled(boolean disabled)
    {
        this.disabled = disabled;
        setModified(true);
    }

    @Transient
    public boolean isLoggedIn()
    {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn)
    {
        this.loggedIn = loggedIn;
    }

// Attributes ----------------------------------------------------------------------------------------------------------

    private static final long serialVersionUID = 5811250114570241327L;

    private String email;
    private String password;
    private String passwordSignature;
    private String firstName;
    private String lastName;
    private boolean disabled;
    private boolean loggedIn;
}
