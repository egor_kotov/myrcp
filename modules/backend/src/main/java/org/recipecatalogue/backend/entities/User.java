package org.recipecatalogue.backend.entities;

/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 27.04.12
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */
/**
 * Represents a user account.
 *
 * @author Rod Odin
 */
public interface User
        extends Entity
{
    /**
     * Introduces business level user violation constraints.
     */
    public static enum UserConstraintViolation
    {
        IllegalEmailFormat, EmailAlreadyExists, IllegalPasswordFormat,
    }

    public String getEmail();
    public void setEmail(String email);

    public String getPassword();
    public void setPassword(String password);

    public String getFirstName();
    public void setFirstName(String firstName);

    public String getLastName();
    public void setLastName(String lastName);

    public boolean isDisabled();
    //public void setDisabled(boolean disabled);

    public boolean isLoggedIn();
    //public void setLoggedIn(boolean loggedIn);
}
