package org.recipecatalogue.backend.impl;

/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 27.04.12
 * Time: 11:24
 * To change this template use File | Settings | File Templates.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

/**
 * Introduces a useful base implementation of the <code>{@link Bean}</code> interface.
 */
public abstract class AbstractBean
        implements Bean
{
    private final EntityManager entityManager;

// Constants -----------------------------------------------------------------------------------------------------------

    protected final static Logger LOG = LoggerFactory.getLogger(AbstractBean.class);

// Constructors --------------------------------------------------------------------------------------------------------

    protected AbstractBean(EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

// Getters/Setters -----------------------------------------------------------------------------------------------------

    public EntityManager getEntityManager()
    {
        return entityManager;
    }
}

