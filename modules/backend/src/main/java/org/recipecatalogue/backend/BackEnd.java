package org.recipecatalogue.backend;

import org.recipecatalogue.backend.impl.Bean;
import org.recipecatalogue.backend.impl.UserManagerImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 25.04.12
 * Time: 10:40
 * To change this template use File | Settings | File Templates.
 */
public class BackEnd
{
    private static final AtomicReference<BackEnd> DEFAULT_BACKEND = new AtomicReference<BackEnd>();

    private EntityManagerFactory entityManagerFactory;
    private EntityManager defaultEntityManager;
    private Map<String, Bean> beans = new HashMap<>();


    protected BackEnd()
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.recipecatalogue.backend.jpa");
        defaultEntityManager = entityManagerFactory.createEntityManager();
        beans.put(UserManager.class.getName(), new UserManagerImpl(defaultEntityManager) {});
    }

    public static BackEnd getDefaultBackend()
    {
        if (DEFAULT_BACKEND.get() == null)
            DEFAULT_BACKEND.compareAndSet(null, new BackEnd());
        return DEFAULT_BACKEND.get();
    }

    public UserManager getUserManager()
    {
        return (UserManager)beans.get(UserManager.class.getName());
    }

    protected void finalize() throws Throwable
    {
        defaultEntityManager.close();
        entityManagerFactory.close();
        super.finalize();
    }

}
