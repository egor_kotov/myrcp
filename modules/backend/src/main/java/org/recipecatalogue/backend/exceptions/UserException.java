package org.recipecatalogue.backend.exceptions;

/**
 * Created by IntelliJ IDEA.
 * User: Slava
 * Date: 27.04.12
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */

import org.recipecatalogue.backend.entities.User;

/**
 * Thrown if something is wrong with user, e.g. if the client tries to add a user with existing username.
 */
public class UserException
        extends EntityException
{
// Constructors --------------------------------------------------------------------------------------------------------

    public UserException(User.UserConstraintViolation constraintViolation)
    {
        super(constraintViolation);
    }

    public UserException(String message, User.UserConstraintViolation constraintViolation)
    {
        super(message, constraintViolation);
    }

    public UserException(String message, Throwable cause, User.UserConstraintViolation constraintViolation)
    {
        super(message, cause, constraintViolation);
    }

    public UserException(Throwable cause, User.UserConstraintViolation constraintViolation)
    {
        super(cause, constraintViolation);
    }

    public UserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, User.UserConstraintViolation constraintViolation)
    {
        super(message, cause, enableSuppression, writableStackTrace, constraintViolation);
    }

// Getters/Setters -----------------------------------------------------------------------------------------------------

    public User.UserConstraintViolation getConstraintViolation()
    {
        return (User.UserConstraintViolation)super.getConstraintViolation();
    }
}

